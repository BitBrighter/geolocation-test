<?php
namespace Src\System;

class DatabaseConnector {

    private $dbConnection = null;

    public function __construct()
    {
        $host = getenv('DB_HOST');
        $port = getenv('DB_PORT');
        $db   = getenv('DB_DATABASE');
        $user = getenv('DB_USERNAME');
        $pass = getenv('DB_PASSWORD');

		$this->dbConnection = new \mysqli($host, $user, $pass, $db);
		
		// Check connection
		if ($this->dbConnection->connect_error) {
			exit("Connection failed: " . $this->dbConnection->connect_error);
		}

        /*try {
            $this->dbConnection = new \PDO(
                "mysql:host=$host;port=$port;charset=utf8mb4;dbname=$db",
                $user,
                $pass
            );
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }*/
    }

    public function getConnection()
    {
        return $this->dbConnection;
    }
}
?>