<?php
namespace Src\Controller;

use Src\TableGateways\LocationGateway;

class LocationController {

    private $db;
    private $requestMethod;
    private $userIP;
    private $LocationGateway;

    public function __construct($db, $requestMethod, $userIP)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->userIP = $userIP;
        $this->LocationGateway = new LocationGateway($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                $response = $this->getLocation($this->userIP);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getLocationsNoIP()
    {
        $result = $this->LocationGateway->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function getLocation($ip)
    {
        $result = $this->LocationGateway->find($ip);
        if (!$result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}
?>