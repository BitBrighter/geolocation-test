<?php
namespace Src\TableGateways;

class LocationGateway {

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findAll()
    {
        $data = [];
        $statement = "SELECT * FROM geolocations";
        $result = $this->db->query($statement);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        } else {
            $data[] = "No results found.";
        }

        return $data;        
    }

    public function find($ip)
    {
        $data = [];

        if ($ip) {
            $statement = "SELECT * FROM geolocations";
            $result = $this->db->query($statement);
            $iplong = ip2long($ip);

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    $high_ip = ip2long($row["ip_stop_range"]); 
                    $low_ip = ip2long($row["ip_start_range"]); 
                    if ($iplong <= $high_ip && $low_ip <= $iplong) {
                        $data[] = [
                            'country' => $row['country_name'],
                            'code' => $row['country_code']
                        ];
                    }
                }
            } else {
                $data[] = "No results found.";
            }
        } else {
            $data[] = "No ipv4 address was specified. Try using locationByIP?ip=YOUR_IP_ADDRESS";
        }

        return $data;        
    }
}
?>