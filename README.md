# README #

This README will document whatever steps are necessary to get the application up and running.

## Clone Repository
```
git clone git@bitbucket.org:BitBrighter/geolocation-test.git

cd geolocation-test
```

## Install Dependancies

```
composer install
```

## Create .env file

Copy .env.example file and rename it to .env. Populate using your MySQL database details.
```
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

## Create Database Table and Seed data from downloaded ZIP/CSV
The below terminal command will check to see if the database table is populated. If its not populated a GeoLocation ZIP file will be downloaded from a remote URL and extracted. The CSV file inside the extracted ZIP will be used to populate the database.
```
php dbseed.php
```

## Run PHP on development server

```
php -S 127.0.0.1:8000 -t public
```

## Test RESTAPI GET Endpoint
```
TYPE: GET
ENDPOINT: /locationByIP
PARAM: ip as String
EXAMPLE: http://127.0.0.1:8000/locationByIP?ip=5.67.105.61
```
