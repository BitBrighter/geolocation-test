<?php
require "../bootstrap.php";
use Src\Controller\LocationController;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );
//error_log(print_r($uri, true));

// all of our endpoints start with /locationByIP
// everything else results in a 404 Not Found
if ($uri[1] !== 'locationByIP') {
    header("HTTP/1.1 404 Not Found");
    exit();
}

// the user ip is, of course, optional and must be a string:
$userIP = null;
if (isset($_REQUEST['ip'])) {
    $userIP = (string) $_REQUEST['ip'];
}


$requestMethod = $_SERVER["REQUEST_METHOD"];

// pass the request method and user IP to the LocationController and process the HTTP request:
$controller = new LocationController($dbConnection, $requestMethod, $userIP);
$controller->processRequest();
?>