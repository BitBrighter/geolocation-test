<?php
require 'bootstrap.php';

// create table if it doesnt exist
$sql = "
    CREATE TABLE IF NOT EXISTS geolocations (
		id INT NOT NULL AUTO_INCREMENT,
		ip_start_range VARCHAR(15) DEFAULT NULL,
		ip_stop_range VARCHAR(15) DEFAULT NULL,
		country_code VARCHAR(2) DEFAULT NULL,
		country_name VARCHAR(100) DEFAULT NULL,
		PRIMARY KEY (id)
	) ENGINE=INNODB;
";
if ($dbConnection->query($sql) === TRUE) {
	error_log( "Table `geolocations` created successfully." );
} else {
	error_log( "Error creating table: " . $dbConnection->error );
}

// check if table is populated
$sql = "SELECT id FROM geolocations";
$result = mysqli_query($dbConnection, $sql);
$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

if(!isset($row['id'])) {

	if (!file_exists('./downloads')) {
		mkdir('./downloads', 0744, true);
	}

	$url = 'https://php-dev-task.s3-eu-west-1.amazonaws.com/GeoIPCountryCSV.zip';
	$zip_file_name = './downloads/'.basename($url);
	if(file_put_contents( $zip_file_name, file_get_contents($url))) {
		
		error_log("Zip file downloading successful!");

		$zip = new ZipArchive;
		if ($zip->open($zip_file_name) === TRUE) {
		
			$zip->extractTo('./downloads/');
			$zip->close();
			error_log("Unzipped Process Successful!");

			unlink($zip_file_name);

		} else {
			error_log("Unzipped Process failed.");
		}	

	} else {
		error_log("Zip file downloading failed.");
	}

	error_log("Starting database import from csv file.");
	$csvfilepath = './downloads/GeoIPCountryWhois.csv';
	if (file_exists($csvfilepath)) {

		$csv = array_map('str_getcsv', file($csvfilepath));
		$state_errors = [];
		foreach($csv as $row){
			
			$sql = "INSERT INTO geolocations (ip_start_range, ip_stop_range, country_code, country_name ) 
			VALUES ('".$row[0]."', '".$row[1]."', '".$row[4]."', '".addslashes($row[5])."')";
			if ($dbConnection->query($sql) !== TRUE) {
				$state_errors[] = "Error: " . $sql . " || " . $dbConnection->error;
			}
			
		}
		if(empty($state_errors)){
			error_log("CSV file imported into database successfully.");
			unlink($csvfilepath);
		} else {
			error_log( print_r($state_errors, true) );
		}

	} else {
		error_log('The file ' . $zipfilepath . ' does not exist, cant populate database table.');
	}	



	// check if zip file or csv exists
	/*$zipexists = false;
	$csvexists = false;
	$zipfilepath = './downloads/GeoIPCountryCSV.zip';
	$csvfilepath = './downloads/GeoIPCountryWhois.csv';

	if (file_exists($zipfilepath)) {
		echo 'The file ' . $zipfilepath . ' exists';
	} else {
		echo 'The file ' . $zipfilepath . ' does not exist';
	}	

	if (file_exists($csvfilepath)) {
		echo 'The file ' . $csvfilepath . ' exists';
	} else {
		echo 'The file ' . $csvfilepath . ' does not exist';
	}*/	
}

$dbConnection->close();

/*try {
    $createTable = $dbConnection->exec($statement);
    echo "Success!\n";
} catch (\PDOException $e) {
    exit($e->getMessage());
}*/
?>